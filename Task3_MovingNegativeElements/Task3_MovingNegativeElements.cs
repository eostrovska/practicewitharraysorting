﻿using System;

namespace Task3_MovingNegativeElements
{
    class Program
    {
        public static void MoveNegativeElementsToTheBeginning(int[] array)
        {
            int temp = 0;

            for (int i = 0; i < array.Length; i++)
            {
                for (int j = 0; j < array.Length - 1; j++)
                {
                    if (array[j] >= 0 && array[j + 1] < 0)
                    {
                        temp = array[j];
                        array[j] = array[j + 1];
                        array[j + 1] = temp;
                    }
                }
            }
        }

        public static void DisplayEditArray(int[] array)
        {
            for (int i = 0; i < array.Length; i++)
            {
                Console.WriteLine(array[i]);
            }
        }

        static void Main(string[] args)
        {

            int[] array = new int[] { 1, 2, 3, 4, 5, 6, 7, 8, 9, -9, -8, -7, -6, -5, -4, -3, -2, -1 };

            Console.WriteLine("Task 3: In a 1D array, move all negative elements to the beginning of the array," +
                              "and the rest - to the end, with preservation of the order of following.");
            Console.WriteLine(new string('-', 60));

            MoveNegativeElementsToTheBeginning(array);
            DisplayEditArray(array);

            Console.ReadKey();
        }
    }
}
