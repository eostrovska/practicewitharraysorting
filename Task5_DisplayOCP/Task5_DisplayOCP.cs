﻿using System;

namespace Task5_DisplayOCP
{
    class Program
    {
        public static void DisplayLetterO(int height, int width)
        {
            for (int i = 0; i <= height; i++)
            {
                for (int j = 0; j <= width; j++)
                {
                    if (((j == 1 || j == height - 1) && i != 0 && i != height) ||
                        ((i == 0 || i == height) && j > 1 && j < width - 1))
                    {
                        Console.Write("*");
                    }
                    else
                    {
                        Console.Write(" ");
                    }
                }
                Console.Write("\n");
            }
            Console.Write("\n");
        }

        public static void DisplayLetterC(int height, int width)
        {
            for (int i = 0; i <= height; i++)
            {
                for (int j = 0; j <= width; j++)
                {
                    if ((j == 1 && (i != 0 && i != height)) || ((i == 0 || i == height) &&
                        (j > 1 && j < width - 1)) || (j == width - 1 && (i == 1 || i == height - 1)))
                    {
                        Console.Write("*");
                    }
                    else
                    {
                        Console.Write(" ");
                    }
                }
                Console.Write("\n");
            }
            Console.Write("\n");
        }

        public static void DisplayLetterP(int height, int width)
        {
            for (int i = 0; i <= height; i++)
            {
                for (int j = 0; j <= width; j++)
                {
                    if (j == 1 || ((i == 0 || i == 3) && j > 0 && j < width - 1) ||
                      ((j == width - 1 || j == 1) && (i == 1 || i == 2)))
                    {
                        Console.Write("*");
                    }
                    else
                    {
                        Console.Write(" ");
                    }
                }
                Console.Write("\n");
            }
        }

        static void Main(string[] args)
        {
            int height = 6;
            int width = 6;

            Console.WriteLine("Task 5: Using functional programming, display letters O, C, P");
            Console.WriteLine(new string('-', 60));

            DisplayLetterO(height, width);
            DisplayLetterC(height, width);
            DisplayLetterP(height, width);

            Console.ReadKey();
        }
    }
}
