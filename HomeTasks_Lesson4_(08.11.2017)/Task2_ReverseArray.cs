﻿using System;

namespace Task2_ReverseArray
{
    class Program
    {
        public static void ReverseArray(int[] array)
        {
            int arrayLength = array.Length;
            int temp;

            for (int i = 0; i < arrayLength / 2; i++)
            {
                temp = array[i];
                array[i] = array[arrayLength - i - 1];
                array[array.Length - i - 1] = temp;
            }
        }

        public static void DisplayReversedArray(int[] array)
        {
            for (int i = 0; i < array.Length; i++)
            {
                Console.WriteLine(array[i]);
            }
        }

        static void Main(string[] args)
        {
            int[] array = new int[] { 1, 2, 3, 4, 5 };
            int[] arrayForReversing = new int[] { 1, 2, 3, 4, 5, 6, 7, 8, 9 };

            Console.WriteLine("Task 2: Reverse 1D array.");
            Console.WriteLine(new string('-', 60));

            ReverseArray(array);
            DisplayReversedArray(array);

            Console.WriteLine("Reverse 1D array through method Array.Reverse (Array)");
            Console.WriteLine(new string('-', 60));

            Array.Reverse(arrayForReversing);
            DisplayReversedArray(arrayForReversing);

            Console.ReadKey();
        }
    }
}
