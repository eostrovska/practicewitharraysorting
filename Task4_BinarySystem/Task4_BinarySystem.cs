﻿using System;
using System.Linq;

namespace Task4_BinarySystem
{
    class Program
    {
        static void Main(string[] args)
        {
            short shortValue = 124;

            Console.WriteLine("Task 4: You have short type value. Display the rank in which the unit is specified" +
                              "if you translate this number into a binary system.");
            Console.WriteLine(new string('-', 60));

            var splittedBinary = ToBinary(shortValue) // convert to binary string
                           .Select(c => int.Parse(c.ToString())) // convert each char to int
                           .Select((x, i) => new { Index = i, Value = x }) // prepare array for grouping
                           .GroupBy(x => x.Index / 4) // group by tetrada rank
                           .Reverse() // reverse for easy reading purpose
                           .Select(x => x.Select(v => v.Value).ToList()) // pack into list of ints
                           .ToList();

            foreach (var group in splittedBinary)
            {
                if (group.Contains(1))
                {
                    Console.WriteLine(String.Join("", group));
                }
            }

            Console.ReadKey();
        }

        private static string ToBinary(short valueToConvert)
        {
            string binVal = Convert.ToString(valueToConvert, 2);
            int bits = 0;
            int bitblock = 4;

            for (int i = 0; i < binVal.Length; i = i + bitblock)
            {
                bits += bitblock;
            }

            return binVal.PadLeft(bits, '0');
        }
    }
}
