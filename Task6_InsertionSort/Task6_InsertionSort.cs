﻿using System;

namespace Task6_InsertionSort
{
    class Program
    {
        public static void InsertionSort(int[] array)
        {
            for (int i = 2; i < array.Length; i++)
            {
                int current = array[i];
                int j = i-1;
                while (j > 0 && current <= array[j])
                {
                    array[j+1] = array[j];
                    j--;
                }
                array[j+1] = current;
            }
        }

        public static void DisplaySortArray(int[] array)
        {
            for (int i = 0; i < array.Length; i++)
            {
                Console.WriteLine(array[i]);
            }
        }

        static void Main(string[] args)
        {
            int[] arrayForSorting = new int[] { 2, 3, 5, 6, 56, 45, 34, 10, 78, 89, 90, 45, 34 };

            Console.WriteLine("Task 6: How to sort array by insertion sort method");
            Console.WriteLine(new string('-', 60));

            InsertionSort(arrayForSorting);
            DisplaySortArray(arrayForSorting);

            Console.ReadKey();

        }
    }
}
