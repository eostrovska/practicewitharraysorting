﻿using System;

namespace Task4_BubbleSort
{
    class Program
    {
        static void BubbleSort(int[] array)
        {
            for (int i = 1; i < array.Length; i++)
            {
                for (int j = i + 1; j < array.Length - 1; j++)
                {
                    if (array[j] > array[j + 1])
                    {
                        int temp = array[j];
                        array[j] = array[j + 1];
                        array[j + 1] = temp;
                    }
                }
            }
        }

        public static void DisplaySortedArray(int[] array)
        {
            for(int i = 0; i < array.Length; i++)
            {
                Console.WriteLine(array[i]);
            }
        }

        static void Main(string[] args)
        {
            int[] arrayForSort = new int[] { 2, 3, 5, 6, 56, 45, 34, 78, 89, 90, 45, 34 };

            Console.WriteLine("Task 4: How to sort array by 'bubble' method");
            Console.WriteLine(new string('-', 60));

            BubbleSort(arrayForSort);
            DisplaySortedArray(arrayForSort);

            Console.ReadKey();
        }
    }
}
