### What is this repository for? ###

* Task 2: Reverse 1D array.
* Task 3: In a 1D array, move all negative elements to the beginning of the array, and the rest - to the end, with preservation of the order of following.
* Task 4: You have short type value. Display the rank in which the unit is specified if you translate this number into a binary system.
* Task 4: How to sort array by 'bubble' method
* Task 5: Using functional programming, display letters O, C, P
* Task 5: How to sort array by selection sort method
* Task 6: How to sort array by insertion sort method

